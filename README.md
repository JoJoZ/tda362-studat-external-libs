# The external libs of TDA362 compiled for the StuDAT computers

As noted
in
[README_LINUX.md](https://bitbucket.org/sverkerr/tda361/src/357e756c023806f1734df1f198c8f9cde06f37ae/README_LINUX.md?at=master&fileviewer=file-view-default),
the labs will not build on StuDAT computers "since some of the
external libraries aren't available.". In this repo, I've compiled
these external libraries so that the labs will indeed build on the
StuDAT computers.

The `external-linux` directory of this repo is equivalent to the
windows-version (`external`) already included in the `sverkerr/tda361` repo.

# Usage

Put the `external-linux` directory of this repo in your project root,
alongside the windows version `external`:

```
$ ls
tda361
$ ls tda361
README.md   README_LINUX.md   CMakeList.txt
external    external_src      lab1
...
$ git clone https://gitlab.com/bryal/tda362-studat-external-libs
...
$ cp -a tda362-studat-external-libs/external-linux tda361/
```

Then update `CMakeList.txt` in your project root to use the bundled
linux libraries instead of trying to find them as system libraries:

file `tda361/CMakeList.txt`:

```
cmake_minimum_required ( VERSION 3.0.2 )

project ( computer-graphics-labs )
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# For windows we use our bundled binaries.
if(WIN32)
    list(APPEND CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/external/embree2")
    list(APPEND CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/external/sdl2")
    list(APPEND CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/external/glew")
    list(APPEND CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/external/glm")
endif(WIN32)


#########################################################
# ADD THE LINES BELOW TO USE BUNDLED BINARIES FOR LINUX #
# VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV #
#########################################################

# We also use bundled binaries for linux
if(UNIX)
    list(APPEND CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/external-linux/embree2")
    list(APPEND CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/external-linux/sdl2")
    list(APPEND CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/external-linux/glew")
    list(APPEND CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/external-linux/glm")
endif(UNIX)

#########################################################
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ #
# ADD THE LINES ABOVE TO USE BUNDLED BINARIES FOR LINUX #
#########################################################


macro(config_build_output)
...
```

or alternatively just replace your `CMakeList.txt` with the
`CMakeList.txt` in this repo, if you haven't made any changes yourself.
